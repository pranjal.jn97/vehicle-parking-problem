# Vehicle parking problem

Design a car parking ticketing system

Problem statement ref - https://docs.google.com/document/d/16WqeWkeRLKCn1JW4hL-4n1Wk-Or_Kt9qUQSC6VawRN0/edit#

# Approach

The system is provided as a command line utility called `car-parking`. You can pass a input file to the command to run the program.

- Have followed Object oriented paradigm for abstraction
- Modular directory structure for readability and easy contributability

# Pre-requisites

- Python3 installed with pip package manager
# How to run

1. Clone the repository `git clone https://gitlab.com/pranjal.jn97/vehicle-parking-problem.git`
2. Go into repository home directory `cd vehicle-parking-problem`
3. Install python package via pip `pip install .`
4. Run command with input file `car-parking -f input.txt`

# Unit Testing

1. Run unit tests with command `python tests/unit/test_parking.py`
