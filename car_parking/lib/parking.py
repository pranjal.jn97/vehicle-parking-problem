"""
Parking Lot
"""
from .logger import LOGGER
from .car import Car

class Parking:
    """
    Parking lot
    """
    def __init__(self, max_slots: int):
        """
        Initializer for parking lot class
        """
        self.slots = [-1] * max_slots
        self.free_slots = max_slots

    def park(self, car: Car):
        """
        Park a car in parking
        """
        if self.free_slots > 0:
            empty_slot_id_index = self._get_empty_slot()

            self.slots[empty_slot_id_index] = car
            self.free_slots -= 1

            return empty_slot_id_index+1

        return -1

    def leave(self, slot_id: int):
        """
        Checkout a car from parking
        """
        if not isinstance(self.slots[slot_id-1], Car):
            LOGGER.info('Sorry! There is no such car in parking lot')

            return False

        self.slots[slot_id-1] = -1
        self.free_slots += 1

        LOGGER.info(f'Car on slot {slot_id} is successfully checked out')

        return True

    def get_slot_id_from_regno(self, regno):
        """
        Get slot_id from Registration number
        """
        for i in range(len(self.slots)):
            if isinstance(self.slots[i], Car) and self.slots[i].registration_no == regno:
                return i+1

        return -1

    def _get_empty_slot(self):
        """
        Gets available slot from parking lot
        """
        for i in range(len(self.slots)):
            if self.slots[i] == -1:
                return i

        return -1

    def get_slot_ids_from_drivers_age(self, drivers_age: int):
        """
        Get slot_ids from driver's age
        """
        slot_ids = []
        for i in range(len(self.slots)):
            if isinstance(self.slots[i], Car) and self.slots[i].driver_age == drivers_age:
                slot_ids.append(i+1)

        return slot_ids

    def get_registrations_nos_from_slot_ids(self, slot_ids: list):
        """
        Get registration number from slot_ids
        """
        regd_no = [self.slots[slot-1].registration_no for slot in slot_ids]

        return ','.join(regd_no)
