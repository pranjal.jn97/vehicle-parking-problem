"""Tests"""
import unittest

from car_parking.lib.parking import Parking
from car_parking.lib.parking import Car


class TestParking(unittest.TestCase):
    """
    Tests parking class
    """
    def test_create_parking_lot(self):
        """
        Tests parking class initialization
        """
        parking_lot = Parking(max_slots=6)
        self.assertEqual(len(parking_lot.slots), 6)

    def test_park_method(self):
        """
        Tests park method
        """
        parking_lot = Parking(max_slots=2)
        car = Car(registration_no='MP-09-HH-1234', driver_age=18)

        slot_id = parking_lot.park(car=car)

        self.assertEqual(parking_lot.free_slots, 1)
        self.assertEqual(slot_id, 1)
        self.assertEqual(parking_lot.slots[slot_id-1], car)

    def test_leave_method(self):
        """
        Tests leave method
        """
        parking_lot = Parking(max_slots=2)
        car = Car(registration_no='MP-09-HH-1234', driver_age=18)
        slot_id = parking_lot.park(car=car)

        self.assertEqual(parking_lot.free_slots, 1)

        parking_lot.leave(slot_id=slot_id)

        self.assertEqual(parking_lot.free_slots, 2)

    def test_slot_id_from_regd_no(self):
        """
        Tests if system returns right slot id when given regd no
        """
        parking_lot = Parking(max_slots=3)
        car = Car(registration_no='MP-09-HH-1234', driver_age=28)
        slot_id = parking_lot.park(car=car)

        derived_slot_id = parking_lot.get_slot_id_from_regno(regno='MP-09-HH-1234')

        self.assertEqual(slot_id, derived_slot_id)

    def test_slot_id_from_drivers_age(self):
        """
        Tests if system returns right slot ids when given driver's age
        """
        parking_lot = Parking(max_slots=3)
        car = Car(registration_no='MP-09-HH-1234', driver_age=28)
        car2 = Car(registration_no='MP-19-HH-1234', driver_age=28)
        slot_ids = [parking_lot.park(car=car), parking_lot.park(car=car2)]

        self.assertEqual(parking_lot.get_slot_ids_from_drivers_age(drivers_age=28), slot_ids)

    def test_regd_nos_from_driver_age(self):
        """
        Tests if system returns right registration numbers when given driver's age as criteria
        """
        parking_lot = Parking(max_slots=3)
        car = Car(registration_no='MP-09-HH-1234', driver_age=28)
        car2 = Car(registration_no='MP-19-HH-1234', driver_age=28)
        slot_ids = [parking_lot.park(car=car), parking_lot.park(car=car2)]

        self.assertEqual(
            parking_lot.get_registrations_nos_from_slot_ids(slot_ids=slot_ids),
            'MP-09-HH-1234,MP-19-HH-1234'
        )


class TestCar(unittest.TestCase):
    """
    Tests Car class
    """
    def test_car_initialization(self):
        """
        Tests if class is initialized fine with right attributes
        """
        car = Car(registration_no='MP-09-HH-1234', driver_age=22)
        self.assertEqual(car.driver_age, 22)
        self.assertEqual(car.registration_no, 'MP-09-HH-1234')


if __name__ == '__main__':
    unittest.main()
