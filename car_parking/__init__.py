"""
Handler
"""
import argparse
from .lib.parking import Parking
from .lib.car import Car
from .lib.logger import LOGGER


def main() -> None:
    """Main runner function."""
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', required=True, dest='input_file', help="Input commands via file")
    args = parser.parse_args()

    if args.input_file:
        with open(args.input_file, 'r') as input_file:
            input_commands = input_file.read().splitlines()

        for command in input_commands:
            LOGGER.info(command)

            if command.startswith('Create_parking_lot'):
                max_slots = int(command.split(' ')[1])

                parking_lot = Parking(max_slots=max_slots)

                LOGGER.info(f'Created parking lot with {max_slots} slots')

            elif command.startswith('Park'):
                registration_no = command.split(' ')[1]
                driver_age = int(command.split(' ')[3])

                car = Car(registration_no=registration_no, driver_age=driver_age)

                slot_id = parking_lot.park(car=car)
                if slot_id == -1:
                    LOGGER.info('Sorry! The parking lot is full at the moment')
                else:
                    LOGGER.info(f'Car parked to slot: {slot_id}')

            elif command.startswith('Leave'):
                exit_slotid = int(command.split(' ')[1])

                parking_lot.leave(slot_id=exit_slotid)

            elif command.startswith('Slot_number_for_car_with_number'):
                registration_no = command.split(' ')[1]

                slot_id = parking_lot.get_slot_id_from_regno(regno=registration_no)

                if slot_id == -1:
                    LOGGER.info(f'No car in parking with number {registration_no}')
                else:
                    LOGGER.info(f'Slot number for car with number {registration_no}: {slot_id}')

            elif command.startswith('Slot_numbers_for_driver_of_age'):
                driver_age = int(command.split(' ')[1])

                slot_ids = parking_lot.get_slot_ids_from_drivers_age(drivers_age=driver_age)
                slot_ids = ','.join([str(slot) for slot in slot_ids])

                LOGGER.info(f'Slot number for drivers of age {driver_age}: {slot_ids}')

            elif command.startswith('Vehicle_registration_number_for_driver_of_age'):
                driver_age = int(command.split(' ')[1])

                slot_ids = parking_lot.get_slot_ids_from_drivers_age(drivers_age=driver_age)
                regd_no = parking_lot.get_registrations_nos_from_slot_ids(slot_ids=slot_ids)

                if regd_no:
                    LOGGER.info(
                        f'Vehicle registration numbers for drivers age {driver_age} are {regd_no}'
                    )
                else:
                    LOGGER.info(f'No car in parking where drivers age is {driver_age}')
    else:
        LOGGER.info('No input commands found')

if __name__ == '__main__':
    main()
