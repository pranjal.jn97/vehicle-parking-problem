"""Logger"""
import logging


logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(process)d %(message)s')

LOGGER = logging.getLogger('parking-lot')
LOGGER.setLevel(logging.INFO)
