"""
Vehicle
"""
from .logger import LOGGER

class Car:
    """
    Abstraction for Car
    """
    def __init__(self, registration_no: str, driver_age: int) -> None:
        """
        Class initializer
        """
        self.registration_no = registration_no
        self.driver_age = driver_age

        LOGGER.debug(
            f'Instantiated car object with registration no {registration_no} and \
                driver age {driver_age}'
        )
