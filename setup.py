#!/usr/bin/env python
"""Package setup."""
from setuptools import setup

setup(
    name='car-parking',
    version='0.1.0',
    description='CLI tool for car parking ticketing system',
    author='Pranjal Jain',
    classifiers=['Programming Language :: Python :: 3 :: Only'],
    py_modules=['car_parking'],
    install_requires=[],
    entry_points="""
    [console_scripts]
    car-parking=car_parking:main
    """,
    packages=['car_parking'],
    include_package_data=True
)
